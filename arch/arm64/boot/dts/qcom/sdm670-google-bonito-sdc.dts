// SPDX-License-Identifier: GPL-2.0-only
/*
 * Device tree for Google Pixel 3a XL with the panel connected to the Samsung
 * Display Controller.
 *
 * Copyright (c) 2023, Richard Acayan. All rights reserved.
 */

/dts-v1/;

#include "sdm670-google-common.dtsi"

/ {
	model = "Google Pixel 3a XL (with SDC panel)";
	compatible = "google,bonito-sdc", "google,bonito", "qcom,sdm670";
};

&battery {
	charge-full-design-microamp-hours = <3700000>;
};

&drv2624 {
	ti,odclamp-voltage = <106>; /* 2.1 Vrms */
};

&framebuffer {
	reg = <0 0x9c000000 0 (1080 * 2160 * 4)>;
	width = <1080>;
	height = <2160>;
	stride = <(1080 * 4)>;
	status = "okay";
};

&panel {
	compatible = "samsung,sofef00-bonito";
	status = "okay";
};

&rmi4_f12 {
	touchscreen-x-mm = <69>;
	touchscreen-y-mm = <137>;
};
